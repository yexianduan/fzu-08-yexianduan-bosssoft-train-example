package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import jdk.nashorn.internal.runtime.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;


/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:13
 * @since
 **/
public class DOMOperation implements XMLOperation<Student> {

    public Document getDocument() {
        //1.创建解析器工厂
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            // 2.通过解析器工厂创建解析器对象
            DocumentBuilder builder = factory.newDocumentBuilder();
            // 3.解析xml文件，得到Document对象
            // 拿到路径
            return builder.parse("src/main/resources/student.tld");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public void flushXML(Document document) {
        // 创建TransformerFactory对象
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        // Transformer类用来将代表xml文件的Document对象装换位某种格式后进行输出
        try {
            // Transformer对象通过TransformerFactory获得
            Transformer transformer = transformerFactory.newTransformer();
            //将处理后的Document对象重新写入 到另一个xml文件
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File("src/main/resources/student.tld"));
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean create(Student object) {
        Integer userId = object.getId();
        Document document = getDocument();
        if (userId != null) {
            Element element = document.createElement("student");
            element.setAttribute("id", userId.toString());

            Element name = document.createElement("name");
            name.setTextContent(object.getName());
            Element age = document.createElement("age");
            age.setTextContent(object.getAge().toString());
            //注意不能连续追加
            element.appendChild(name);
            element.appendChild(age);

            document.getFirstChild().appendChild(element);

            //刷写
            flushXML(document);
            return true;
        }
        return false;
    }

    /**
     * 删除 按照id删除吧
     *
     * @param object
     * @return
     */
    @Override
    public boolean remove(Student object) {
        Document document = getDocument();
        Integer userId = object.getId();
        if (userId != null) {
            NodeList students = document.getElementsByTagName("student");
            for (int i = 0; i < students.getLength(); i++) {
                Element item = (Element) students.item(i);
                if (userId.equals(Integer.parseInt(item.getAttribute("id")))) {
                    item.getParentNode().removeChild(item);
                    //刷新
                    flushXML(document);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 按照给定的Id 更新
     *
     * @param object
     * @return
     */
    @Override
    public boolean update(Student object) {
        Document document = getDocument();
        NodeList students = document.getElementsByTagName("student");
        for (int i = 0; i < students.getLength(); i++) {
            Element item = (Element) students.item(i);
            if (object.getId().equals(Integer.parseInt(item.getAttribute("id")))) {
                item.getElementsByTagName("name").item(0).setTextContent(object.getName());
                item.getElementsByTagName("age").item(0).setTextContent(String.valueOf(object.getAge()));
                flushXML(document);
                return true;
            }
        }
        return false;

    }

    /**
     * 假设按照封装的id查 默认id不重复
     *
     * @param object
     * @return
     */
    @Override
    public Student query(Student object) {
        if (object.getId() != null) {
            Document document = getDocument();
            NodeList nodeList = document.getElementsByTagName("student");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element item = (Element) nodeList.item(i);
                if (object.getId().equals(Integer.parseInt(item.getAttribute("id")))) {
                    //获取名字
                    NodeList nameList = item.getElementsByTagName("name");
                    String name = nameList.item(0).getTextContent();
                    object.setName(name);
                    //
                    NodeList ageList = item.getElementsByTagName("age");
                    String age = ageList.item(0).getTextContent();
                    object.setAge(Integer.parseInt(age));

                    return object;
                }
            }
        }
        return null;
    }


}
