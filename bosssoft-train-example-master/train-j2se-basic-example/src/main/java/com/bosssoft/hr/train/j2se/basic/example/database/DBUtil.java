package com.bosssoft.hr.train.j2se.basic.example.database;

import java.sql.*;

/**
 * @description:  我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
public final class DBUtil {
    private static final String URL = "jdbc:mysql://127.0.0.1:3308/bosssoft?useUnicode=true&characterEncoding=utf-8&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static Connection conn = null;
    private static ResultSet rs = null;
    private static PreparedStatement ps = null;

    private DBUtil() {

    }


    public static Connection createConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *  如果需要参数的你可以选择overload 改方法
     * @param sql
     * @return
     */
    public static ResultSet executeQuery(String sql){
        if (conn != null) {
            try {
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rs;
    }
    public static int executeUpdate(String sql) {

        int result = -1;

        try {
            ps = conn.prepareStatement(sql);
            //执行
            result = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
