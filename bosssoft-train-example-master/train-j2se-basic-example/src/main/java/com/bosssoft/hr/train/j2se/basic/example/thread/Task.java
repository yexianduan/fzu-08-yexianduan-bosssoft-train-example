package com.bosssoft.hr.train.j2se.basic.example.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Task implements Runnable{

    //线程编号
    private int id;
    public Task(int id){
        this.id=id;
    }
    @Override
    public void run() {
        int runTime=(int) Math.random()*5+1;
        log.info("task" + id + " is running...");
        try {
            Thread.sleep(runTime*100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("task" + id + " is over...");
    }
}
