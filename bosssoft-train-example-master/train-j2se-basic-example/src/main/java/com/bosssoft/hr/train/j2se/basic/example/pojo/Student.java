package com.bosssoft.hr.train.j2se.basic.example.pojo;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:07
 * @since
 **/
public class Student extends User {
    private Integer age;
    public Student(int id,String name,int age){
        super(id,name);
        this.age=age;
    }
    public Student(int id){
        super(id);
    }
    public void setAge(int age){
        this.age=age;
    }
    public Integer getAge(){
        return age;
    }
}
