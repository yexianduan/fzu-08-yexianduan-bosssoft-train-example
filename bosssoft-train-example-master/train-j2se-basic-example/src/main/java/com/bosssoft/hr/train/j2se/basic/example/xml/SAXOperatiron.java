package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:10
 * @since
 **/
public class SAXOperatiron extends DefaultHandler {
    //存放遍历集合
    private  Student student;
    private String tagName;

    /**
     * 假设按照封装的id查 默认id不重复
     *
     * @param object
     * @return
     */
    public Student query(Student object) {
        student=object;
        if (object.getId() != null) {
            try {
                File inputFile = new File("src/main/resources/student.tld");
                // 创建一个SAX解析工厂
                SAXParserFactory factory = SAXParserFactory.newInstance();
                // 创建一个SAX转换工具
                SAXParser saxParser = factory.newSAXParser();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("student")){
            student=new Student(57035);
            //获取student节点上的id属性值
            student.setId(Integer.parseInt(attributes.getValue(0)));
            //获取student节点上的Age属性值
            student.setAge(Integer.parseInt(attributes.getValue(1)));
        }
        tagName=attributes.getValue(0);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
    }
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        String str = new String(ch, start, length);
        if (tagName.equals(str)) {
            System.out.print(str);
        }
    }
}


