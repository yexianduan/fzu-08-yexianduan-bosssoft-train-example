package com.bosssoft.hr.train.j2se.basic.example.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPool {


    public ThreadPoolExecutor testThreadPool() {
        ThreadPoolExecutor executor =
                new ThreadPoolExecutor(5,
                        Integer.MAX_VALUE,
                        60,
                        TimeUnit.SECONDS,
                        new LinkedBlockingQueue<Runnable>());
        return executor;
    }
}
