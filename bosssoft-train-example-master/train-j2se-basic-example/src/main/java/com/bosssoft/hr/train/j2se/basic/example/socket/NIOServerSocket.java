package com.bosssoft.hr.train.j2se.basic.example.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:25
 * @since
 **/
public class NIOServerSocket implements Starter{
    @Override
    public boolean start() {
        //创建serverSocketChannel，监听8888端口
        try(ServerSocketChannel serverSocketChannel=ServerSocketChannel.open()){
            serverSocketChannel.socket().bind(new InetSocketAddress(8888));
            //设置为非阻塞模式
            serverSocketChannel.configureBlocking(false);
            //为serverChannel注册selector
            Selector selector = Selector.open();
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

            while (selector.select() > 0) {
                selector.select();
                //获取selectionKeys并处理
                Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = keyIterator.next();
                    try{
                        //连接请求
                        if(key.isAcceptable()){
                            //获取channel
                            SocketChannel socketChannel = ((ServerSocketChannel) key.channel()).accept();
                            //非阻塞
                            socketChannel.configureBlocking(false);
                            //注册selector
                            socketChannel.register(key.selector(), SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                        }
                        //读请求
                        if (key.isReadable()) {
                            SocketChannel socketChannel = (SocketChannel) key.channel();
                            ByteBuffer buffer = (ByteBuffer) key.attachment();
                            String receivedStr = "";
                            //将channel改为读取状态
                            buffer.flip();
                            //按照编码读取数据
                            receivedStr = Charset.forName("UTF-8").newDecoder().decode(buffer).toString();
                            buffer.clear();
                            //返回数据给客户端
                            buffer = buffer.put(("hello client").getBytes("UTF-8"));
                            //读取模式
                            buffer.flip();
                            socketChannel.write(buffer);
                            socketChannel.close();

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //处理完后移除当前使用的key
                    keyIterator.remove();
                }
            }
            return true;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
