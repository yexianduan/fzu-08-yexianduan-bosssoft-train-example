package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class TreeSetExampleImplTest {
    private TreeSetExampleImpl set=new TreeSetExampleImpl();
    User user=new User(57032,"叶先锻");
    User user2=new User(57034,"XXX");
    @Test
    public void sort() {
        User users[] = {user, user2 };
        User users1[] = {user2,user };
        assertArrayEquals(users,set.sort(users1));
    }
}