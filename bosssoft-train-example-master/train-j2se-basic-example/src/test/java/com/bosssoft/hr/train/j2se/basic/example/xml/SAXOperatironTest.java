package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SAXOperatironTest {
    private SAXOperatiron saxOperatiron;

    @Before
    public void setUp() throws Exception {
        saxOperatiron=new SAXOperatiron();
    }

    @After
    public void tearDown() throws Exception {
        saxOperatiron=null;
    }

    @Test
    public void query() {
        Student student=new Student(57032,"叶先锻",21);
        saxOperatiron.query(student);
    }
}