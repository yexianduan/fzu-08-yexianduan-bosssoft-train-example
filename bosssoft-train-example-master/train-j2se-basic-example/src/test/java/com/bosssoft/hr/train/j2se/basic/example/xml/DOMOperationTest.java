package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DOMOperationTest {
    private DOMOperation domOperation;
    private Student student;

    @Before
    public void setUp() throws Exception {
        domOperation=new DOMOperation();
        student=new Student(57032,"叶先锻",21);
    }

    @After
    public void tearDown() throws Exception {
        domOperation=null;
        student=null;
    }

    @Test
    public void create() {
        assertTrue(domOperation.create(student));
    }

    @Test
    public void remove() {
        student.setId(57033);
        assertTrue(domOperation.create(student));
        assertTrue(domOperation.remove(student));
    }

    @Test
    public void update() {
        assertTrue(domOperation.create(student));
        student.setId(57034);
        assertTrue(domOperation.update(student));
    }

    @Test
    public void query() {
        Student student=new Student(57031);
        assertEquals(this.student, domOperation.query(student));
    }
}