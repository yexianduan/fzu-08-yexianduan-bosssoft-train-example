package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapExampleImplTest {
    private HashMapExampleImpl map;
    private Role role;
    private Resource resource;

    @Before
    public void setUp() throws Exception {
        map = new HashMapExampleImpl();
        Role role= new Role(53702,"叶先锻");
        resource=new Resource(1,"button");
    }

    @After
    public void tearDown() throws Exception {
        map=null;
        role=null;
        resource=null;
    }

    @Test
    public void put() {
        Resource resource=map.put(role,this.resource);
        assertEquals(resource,this.resource);
    }

    @Test
    public void remove() {
        map.put(role,this.resource);
        assertEquals(resource,map.remove(role));
    }

    @Test
    public void containsKey() {
        assertFalse(map.containsKey(role));
        map.put(role,this.resource);
        assertTrue(map.containsKey(role));
    }

    @Test
    public void visitByEntryset() {
        map.put(role,this.resource);
        map.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        map.put(role,this.resource);
        map.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        map.put(role,this.resource);
        map.visitByValues();
    }
}