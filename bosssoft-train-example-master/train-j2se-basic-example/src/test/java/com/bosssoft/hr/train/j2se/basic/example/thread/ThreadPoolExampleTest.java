package com.bosssoft.hr.train.j2se.basic.example.thread;

import java.util.concurrent.ThreadPoolExecutor;

import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

@Slf4j
public class ThreadPoolExampleTest {


    @Test
    public void testThreadPool() {
        ThreadPool threadPool = new ThreadPool();
        ThreadPoolExecutor executor = threadPool.testThreadPool();
        //往线程池中循环提交线程
        for (int i = 0; i < 10; i++) {
            //创建线程类对象
            Task myTask = new Task(i);
            //开启线程
            executor.execute(myTask);
            log.info("线程池中线程数目：" +executor.getPoolSize() +
                    "，队列中等待执行的任务数目："+executor.getQueue().size() +
                    "，已执行完的任务数目："+executor.getCompletedTaskCount());

        }
    }
}
