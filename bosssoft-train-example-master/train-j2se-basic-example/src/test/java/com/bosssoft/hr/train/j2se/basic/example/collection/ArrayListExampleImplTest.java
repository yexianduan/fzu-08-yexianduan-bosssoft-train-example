package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



import static org.junit.Assert.*;

public class ArrayListExampleImplTest {
    private ArrayListExampleImpl users;
    User user;

    @Before
    public void setUp() throws Exception {
        users=new ArrayListExampleImpl();
        user=new User(53702,"叶先锻");
    }

    @After
    public void tearDown() throws Exception {
        users=null;
        user=null;
    }

    @Test
    public void append() {
        assertEquals(true,users.append(user));
    }

    @Test
    public void get() {
        assertEquals(true,users.append(user));
        assertEquals(user,users.get(0));
        assertNull(null,users.get(3));
    }

    @Test
    public void insert() {
        User user=new User(53703,"XXX");
        assertEquals(true,users.insert(0,user));
        assertEquals(false,users.insert(-1,user));
    }

    @Test
    public void remove() {
        User user=new User(53704,"XXX");
        assertEquals(true,users.append(user));

        assertEquals(true,users.remove(0));
        assertEquals(false,users.remove(0));

    }

    @Test
    public void listByIndex() {
        assertEquals(true,users.append(user));
        assertEquals(true,users.append(user));

        users.listByIndex();
    }

    @Test
    public void listByIterator() {
        assertEquals(true,users.append(user));
        assertEquals(true,users.append(user));

        users.listByIterator();
    }

    @Test
    public void toArray() {
        assertEquals(true,users.append(user));
        assertEquals(true,users.append(user));

        User[] users1=new User[]{user,user};
        assertArrayEquals(users1,users.toArray());
    }

    @Test
    public void sort() {
        User user1=new User(53704,"XXX");
        assertEquals(true,users.append(user));
        assertEquals(true,users.append(user1));

        users.sort();
        assertEquals(user,users.get(0));
        assertEquals(user1,users.get(1));

    }

    @Test
    public void sort2() {
        User user1=new User(53704,"XXX");
        assertEquals(true,users.append(user));
        assertEquals(true,users.append(user1));

        users.sort();
        assertEquals(user,users.get(0));
        assertEquals(user1,users.get(1));
    }
}