<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>登录页面</title>
    <link rel="stylesheet" href="js/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="js/jquery-3.5.1.js"></script>
    <script src="js/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container m-auto">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <h2 class="text-center">登录</h2><br><br>
                <div class="form-group">
                    <label for="account">账号</label>
                    <input type="text" class="form-control" id="account" name="code" placeholder="请输入账号" required>
                </div>
                <div class="form-group">
                    <label for="password">密码</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="请输入密码" required>
                </div>
                <button id="login" type="button" class="btn btn-block btn-primary">登录</button>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">提示信息</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    账号或密码错误
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-block btn-primary" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $("#login").click(function(){
                let account = $("#account").val();
                let password = $("#password").val();
                $.ajax({
                    url:"/user/login.do",
                    data: {
                        "account" : account,
                        "password" : password
                    },
                    type: "GET",
                    responseType: "json",
                    success:function(data){
                        let result = $.parseJSON(data);
                        if (result.status === "success") {
                            window.location.href = "/user/display.do";
                        } else {
                            $("#myModal").modal("show");
                        }
                    }
                });
            });
        })
    </script>
</body>
</html>
