<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="boss" uri="http://boss.xx.com" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>查询结果</title>
    <link rel="stylesheet" href="../../js/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="../../js/jquery-3.5.1.js"></script>
    <script src="../../js/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container m-auto">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <form action="${pageContext.request.contextPath}/user/display.do" type="POST">
                <table class="table table-condensed table-dark">
                    <caption style="font-size:30px;text-align:center;caption-side:top">查询结果</caption>
                    <thead class="thead-light">
                        <tr>
                            <th id="id">ID</th>
                            <th id="name">姓名</th>
                            <th id="account">账号</th>
                            <th id="password">密码</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${sessionScope.resultSet}" var="user" >
                            <tr>
                                <td>${user.id}</td>
                                <td>${user.name}</td>
                                <td>${user.code}</td>
                                <td>${user.password}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-block btn-primary">确定</button>
            </form>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>
</body>
</html>
