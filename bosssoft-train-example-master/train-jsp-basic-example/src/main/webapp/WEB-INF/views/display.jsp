<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="boss" uri="http://boss.xx.com" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>用户展示</title>
    <link rel="stylesheet" href="../../js/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="../../js/jquery-3.5.1.js"></script>
    <script src="../../js/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container m-auto">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <h2 class="text-center">用户展示</h2><br><br>
                <boss:user/> <label class="text-right">在线人数：${applicationScope.onlineCount}</label>
                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#insertModal">添加</button>
                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#removeModal">删除</button>
                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#updateModal">修改</button>
                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#queryModal">查询</button>
            </div>
            <div class="col-sm-4"></div>
        </div>
        <div class="modal fade" id="insertModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">添加用户</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="idAdd">ID</label>
                            <input type="text" class="form-control" id="idAdd" name="code" placeholder="请输入ID" required>
                        </div>
                        <div class="form-group">
                            <label for="nameAdd">姓名</label>
                            <input type="text" class="form-control" id="nameAdd" name="code" placeholder="请输入名字" required>
                        </div>
                        <div class="form-group">
                            <label for="accountAdd">账号</label>
                            <input type="text" class="form-control" id="accountAdd" name="code" placeholder="请输入账号" required>
                        </div>
                        <div class="form-group">
                            <label for="passwordAdd">密码</label>
                            <input type="password" class="form-control" id="passwordAdd" name="password" placeholder="请输入密码" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="insert" type="button" class="btn btn-block btn-primary" data-dismiss="modal">确定</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="removeModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">删除用户</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="idRemove">ID</label>
                            <input type="text" class="form-control" id="idRemove" name="code" placeholder="请输入ID" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="remove" type="button" class="btn btn-block btn-primary" data-dismiss="modal">确认</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="updateModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">修改用户</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="idUpdate">ID</label>
                            <input type="text" class="form-control" id="idUpdate" placeholder="请输入ID" required>
                        </div>
                        <div class="form-group">
                            <label for="nameUpdate">姓名</label>
                            <input type="text" class="form-control" id="nameUpdate" placeholder="请输入名字" required>
                        </div>
                        <div class="form-group">
                            <label for="accountUpdate">账号</label>
                            <input type="text" class="form-control" id="accountUpdate" placeholder="请输入账号" required>
                        </div>
                        <div class="form-group">
                            <label for="passwordUpdate">密码</label>
                            <input type="password" class="form-control" id="passwordUpdate" placeholder="请输入密码" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="update" type="button" class="btn btn-block btn-primary" data-dismiss="modal">确认</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="queryModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">查询用户</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="condition">条件</label>
                            <input type="text" class="form-control" id="condition" placeholder="请输入查询条件">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="query" type="submit" class="btn btn-block btn-primary" data-dismiss="modal">确认</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="successModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h4 class="h4 text-center">操作成功</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-block btn-primary" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="failModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h4 class="h4 text-center">操作失败</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-block btn-primary" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $("#insert").click(function(){
                let id = $("#idAdd").val();
                let name = $("#nameAdd").val();
                let account = $("#accountAdd").val();
                let password = $("#passwordAdd").val();
                $.ajax({
                    url:"/user/add.do",
                    data: {
                        "id" : id,
                        "name" : name,
                        "account" : account,
                        "password" : password
                    },
                    type: "GET",
                    responseType: "json",
                    success:function(data){
                        let result = $.parseJSON(data);
                        if (result.status === "success") {
                            $("#successModal").modal("show");
                        } else {
                            $("#failModal").modal("show");
                        }
                    }
                });
            });
            $("#update").click(function(){
                let id = $("#idUpdate").val();
                let name = $("#nameUpdate").val();
                let account = $("#accountUpdate").val();
                let password = $("#passwordUpdate").val();
                $.post("/user/update.do",
                    {
                        "id" : id,
                        "name" : name,
                        "account" : account,
                        "password" : password
                    }
                , function (data) {
                        let result = $.parseJSON(data);
                        if (result.status === "success") {
                            $("#successModal").modal("show");
                        } else {
                            $("#failModal").modal("show");
                        }
                });
            });
            $("#remove").click(function() {
                let id = $("#idRemove").val();
                $.ajax({
                    url: "/user/remove.do",
                    data: {
                        "id": id
                    },
                    type: "GET",
                    responseType: "json",
                    success: function (data) {
                        let result = $.parseJSON(data);
                        if (result.status === "success") {
                            $("#successModal").modal("show");
                        } else {
                            $("#failModal").modal("show");
                        }
                    }
                });
            });
            $("#query").click(function() {
                let condition = $("#condition").val();
                $.ajax({
                    url: "/user/query.do",
                    data: {
                        "condition": condition
                    },
                    type: "GET",
                    success:function(){
                        window.location.href = "/user/result.do";
                    }
                });
            });
        })
    </script>
</body>
</html>
