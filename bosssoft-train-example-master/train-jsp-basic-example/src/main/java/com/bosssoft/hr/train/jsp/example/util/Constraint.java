package com.bosssoft.hr.train.jsp.example.util;

/**
 *
 * @description 常量类
 * @author Xx
 * @create 2020-05-28 21:13
 * @since 1.0
 */
public class Constraint {

    public static final String MESSAGE = "Message: ";

    public static final String ONLINE_COUNT = "onlineCount";

    public static final String RESULT_SET = "resultSet";

    public static final String USER_ID = "id";
    public static final String USER_NAME = "name";
    public static final String USER_ACCOUNT = "account";
    public static final String USER_PASSWORD = "password";

    // 成功
    public static final String SUCCESS = "success";
    // 失败
    public static final String FAIL = "fail";

    // 业务异常码
    public static final String INSERT_ERROR = "000001";
    public static final String REMOVE_ERROR = "000002";
    public static final String UPDATE_ERROR = "000003";
    public static final String QUERY_ERROR = "000004";
    public static final String LOGIN_ERROR = "000005";

    private Constraint() {
    }
}
