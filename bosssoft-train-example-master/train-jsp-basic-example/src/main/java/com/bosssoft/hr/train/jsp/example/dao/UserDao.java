package com.bosssoft.hr.train.jsp.example.dao;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;

import java.util.List;

/**
 * @description 对user表的增删改查
 * @author Administrator
 * @create 2020-05-30 10:38
 * @since 1.0
 */
public interface UserDao {
    /**
     * 插入记录
     *
     * @param user 数据
     * @return 返回影响的行数
     */
    int insert(User user);

    /**
     * 删除记录
     *
     * @param id 用户id
     * @return 返回影响的行数
     */
    int deleteById(Integer id);

    /**
     * 更新记录
     *
     * @param user 数据
     * @return 返回影响的行数
     */
    int update(User user);

    /**
     * 查询记录
     *
     * @param queryCondition 条件
     * @return 结果
     */
    List<User> queryByCondition(Query queryCondition);
}
