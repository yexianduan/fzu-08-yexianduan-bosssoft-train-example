package com.bosssoft.hr.train.jsp.example.controller.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;

/**
 * @description URI信息
 * @author Xx
 * @create 2020/6/1 11:23
 * @since 1.0
 */
@Data
@AllArgsConstructor
public class URIInfo {
    //控制器键
    private String controllerId;

    //控制器方法
    private Method method;

}
