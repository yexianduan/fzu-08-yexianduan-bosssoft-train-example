package com.bosssoft.hr.train.jsp.example.dao.impl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.util.List;

/**
 * @description
 * @author Xx
 * @create 2020-05-30 10:42
 * @since 1.0
 */
public class UserDaoImpl implements UserDao {

    @Override
    public int insert(User user) {
        String sql = "insert into user (id, name, code, password) values (?,?,?,?)";
        return DBUtil.executeUpdate(sql, user.getId(), user.getName(),
                                        user.getCode(), user.getPassword());
    }

    @Override
    public int deleteById(Integer id) {
        String sql = "delete from user where id = ?";
        return DBUtil.executeUpdate(sql, id);
    }

    @Override
    public int update(User user) {
        String sql = "update user set name = ?, code = ?, password = ? where id = ?";
        return DBUtil.executeUpdate(sql, user.getName(), user.getCode(),
                                        user.getPassword(), user.getId());
    }

    @Override
    public List<User> queryByCondition(Query query) {
        return query.query();
    }
}
