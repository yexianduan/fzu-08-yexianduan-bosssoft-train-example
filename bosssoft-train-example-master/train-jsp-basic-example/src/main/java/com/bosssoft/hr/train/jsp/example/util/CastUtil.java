package com.bosssoft.hr.train.jsp.example.util;

/**
 *
 * @description 类型转换工具类
 * @author Xx
 * @create 2020/6/4 9:02
 * @since 1.0
 */
public final class CastUtil {

    private CastUtil() {}

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object object) {
        return (T) object;
    }
}
