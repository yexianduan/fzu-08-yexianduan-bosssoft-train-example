package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.controller.annotation.Controller;
import com.bosssoft.hr.train.jsp.example.controller.annotation.RequestMapping;
import com.bosssoft.hr.train.jsp.example.pojo.*;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;
import com.bosssoft.hr.train.jsp.example.util.Constraint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @description 用户控制器
 * @author Xx
 * @create 2020-05-29 14:07
 * @since 1.0
 */
@Controller
@RequestMapping("/user")
public class UserController implements UserApi {

    private final UserService userService;

    public UserController() {
        userService = new UserServiceImpl();
    }

    @RequestMapping("/add.do")
    public String addUser(HttpServletRequest request, HttpServletResponse response) {
        Integer id = Integer.valueOf(request.getParameter(Constraint.USER_ID));
        String name = request.getParameter(Constraint.USER_NAME);
        String account = request.getParameter(Constraint.USER_ACCOUNT);
        String password = request.getParameter(Constraint.USER_PASSWORD);
        return save(new User(id, name, account, password));
    }

    @RequestMapping("/update.do")
    public String updateUser(HttpServletRequest request, HttpServletResponse response) {
        Integer id = Integer.valueOf(request.getParameter(Constraint.USER_ID));
        String name = request.getParameter(Constraint.USER_NAME);
        String account = request.getParameter(Constraint.USER_ACCOUNT);
        String password = request.getParameter(Constraint.USER_PASSWORD);
        return update(new User(id, name, account, password));
    }

    @RequestMapping("/remove.do")
    public String removeUser(HttpServletRequest request, HttpServletResponse response) {
        Integer id = Integer.valueOf(request.getParameter(Constraint.USER_ID));
        return remove(new User(id, null, null, null));
    }

    @RequestMapping("/query.do")
    public List<User> queryUser(HttpServletRequest request, HttpServletResponse response) {
        String condition = request.getParameter("condition");
        Query query;
        if (condition.equals("")) {
            query = new Query();
        } else if (condition.matches("^[0-9]*$")) {
            query = new QueryById(Integer.valueOf(condition));
        } else if (condition.matches("^[A-Za-z]+$")) {
            query = new QueryByFuzzyName(condition);
        } else if (condition.matches("^[A-Za-z0-9]{4,40}$")) {
            query = new QueryByCode(condition);
        } else {
            query = new Query();
        }
        return query(query);
    }

    @RequestMapping("/login.do")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        String account = request.getParameter(Constraint.USER_ACCOUNT);
        String password = request.getParameter(Constraint.USER_PASSWORD);
        boolean flag = userService.authentication(new User(account, password));
        return flag ? Constraint.SUCCESS : Constraint.FAIL;
    }

    @RequestMapping("/display.do")
    public String display(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/display.jsp").forward(request, response);
        return null;
    }

    @RequestMapping("/result.do")
    public String result(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/result.jsp").forward(request, response);
        return null;
    }

    @Override
    public String save(User user) {
        boolean save = userService.save(user);
        return save ? Constraint.SUCCESS : Constraint.FAIL;
    }

    @Override
    public String remove(User user) {
        boolean save = userService.remove(user);
        return save ? Constraint.SUCCESS : Constraint.FAIL;
    }

    @Override
    public String update(User user) {
        boolean save = userService.update(user);
        return save ? Constraint.SUCCESS : Constraint.FAIL;
    }

    @Override
    public List<User> query(Query condition) {
        return userService.queryByCondition(condition);
    }
}
