package com.bosssoft.hr.train.jsp.example.service.impl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.dao.impl.UserDaoImpl;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.QueryByCode;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.util.Constraint;

import java.util.List;

/**
 * @description
 * @author Xx
 * @create 2020-05-30 10:24
 * @since 1.0
 */
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDaoImpl();
    }

    @Override
    public boolean save(User user) {
        try {
            return userDao.insert(user) > 0;
        } catch (Exception e) {
            throw new BusinessException(Constraint.INSERT_ERROR, e.getMessage(), e);
        }
    }

    @Override
    public boolean remove(User user) {
        try {
            return userDao.deleteById(user.getId()) > 0;
        } catch (Exception e) {
            throw new BusinessException(Constraint.REMOVE_ERROR, e.getMessage(), e);
        }
    }

    @Override
    public boolean update(User user) {
        try {
            return userDao.update(user) > 0;
        } catch (Exception e) {
            throw new BusinessException(Constraint.UPDATE_ERROR, e.getMessage(), e);
        }
    }

    @Override
    public List<User> queryByCondition(Query query) {
        try {
            return userDao.queryByCondition(query);
        } catch (Exception e) {
            throw new BusinessException(Constraint.QUERY_ERROR, e.getMessage(), e);
        }
    }

    @Override
    public boolean authentication(User user) {
        try {
            List<User> users = new QueryByCode(user.getCode()).query();
            return users.size() == 1 && users.get(0).getPassword().equals(user.getPassword());
        } catch (Exception e) {
            throw new BusinessException(Constraint.LOGIN_ERROR, e.getMessage(), e);
        }
    }
}
