package com.bosssoft.hr.train.jsp.example.pojo;

import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @description User查询对象
 * @author Xx
 * @create 2020-05-29 14:09
 * @since 1.0
 */
public class Query {

    protected String sql;

    public Query() {
        this.sql = "select * from user";
    }

    public List<User> query(){
        return transform();
    }

    /**
     * 结果集映射
     *
     * @return 结果集
     */
    protected List<User> transform() {
        List<Map<String, Object>> mapList = DBUtil.executeQuery(sql);
        List<User> list = new ArrayList<>();
        for (Map<String, Object> map : mapList) {
            User user = new User();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object val = entry.getValue();
                switch (key) {
                    case "id":
                        user.setId((Integer) val);
                        break;
                    case "name":
                        user.setName((String) val);
                        break;
                    case "code":
                        user.setCode((String) val);
                        break;
                    case "password":
                        user.setPassword((String) val);
                        break;
                    default:
                }
            }
            list.add(user);
        }
        return list;
    }
}
