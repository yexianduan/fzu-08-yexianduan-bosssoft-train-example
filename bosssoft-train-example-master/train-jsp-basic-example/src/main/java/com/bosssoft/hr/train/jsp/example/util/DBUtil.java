package com.bosssoft.hr.train.jsp.example.util;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 * @author Xx
 * @create 2020-05-28 20:45
 * @since 1.0
 */
@Slf4j
public final class DBUtil {

    private static final String URL;
    private static final String DRIVER;
    private static final String USERNAME;
    private static final String PASSWORD;

    static {
        URL = "jdbc:mysql://47.98.246.225:3306/train?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai";
        DRIVER = "com.mysql.cj.jdbc.Driver";
        USERNAME = "root";
        PASSWORD = "WYZ980417";
    }

    private DBUtil() {}

    /**
     * 获取数据库连接
     *
     * @return 连接
     */
    private static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            log.warn(Constraint.MESSAGE + "{}", e.getMessage());
        }
        return connection;
    }

    /**
     * 执行查询语句
     *
     * @param sql sql
     * @param args 参数
     * @return 结果集
     */
    public static List<Map<String, Object>> executeQuery(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet set = null;
        List<Map<String, Object>> list = null;
        try {
            while (connection == null) {
                connection = getConnection();
            }

            statement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                statement.setObject(i + 1, args[i]);
            }
            set = statement.executeQuery();

            // 结果集的列数
            int count = set.getMetaData().getColumnCount();
            list = new ArrayList<>();
            while (set.next()) {
                Map<String, Object> map = new HashMap<>();
                for (int i = 0; i < count; i++) {
                    String name = set.getMetaData().getColumnName(i + 1);
                    map.put(name, set.getObject(name));
                }
                list.add(map);
            }
        } catch (SQLException e) {
            log.warn(Constraint.MESSAGE + "{}", e.getMessage());
        } finally {
            close(connection, statement, set);
        }
        return list;
    }

    /**
     * 执行增删改语句
     *
     * @param sql sql
     * @param args 参数
     * @return 受影响的行数
     */
    public static int executeUpdate(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            while (connection == null) {
                connection = getConnection();
            }

            statement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                statement.setObject(i + 1, args[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            log.warn(Constraint.MESSAGE + "{}", e.getMessage());
        } finally {
            close(connection, statement, null);
        }
        return 0;
    }

    /**
     * 释放资源
     *
     * @param connection 连接
     * @param statement Statement对象
     * @param set 结果集
     */
    private static void close(Connection connection,PreparedStatement statement,ResultSet set) {
        try {
            if (set != null) {
                set.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            log.warn(Constraint.MESSAGE + "{}" , e.getMessage());
        }
    }
}
