package com.bosssoft.hr.train.jsp.example.controller.util;

import com.bosssoft.hr.train.jsp.example.controller.annotation.Controller;
import com.bosssoft.hr.train.jsp.example.controller.annotation.RequestMapping;
import com.bosssoft.hr.train.jsp.example.controller.entity.URIInfo;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

/**
 * @author Xx
 * @description
 * @create 2020/6/1 11:54
 * @since 1.0
 */
public final class ReflectUtil {

    private ReflectUtil() {}

    /**
     * 根据包名反射解析该包下所有的控制器
     *
     * @param packageName 包名
     * @return 控制器
     */
    public static Map<String, Object> analysisController(String packageName) throws URISyntaxException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        Map<String, Object> controllersMap = new HashMap<>();
        ClassLoader classLoader = ReflectUtil.class.getClassLoader();
        // 包目录名
        String packageDirectory = packageName.replace(".", "/");
        URL resource = classLoader.getResource(packageDirectory);
        // 包目录
        File directory = new File(Objects.requireNonNull(resource).toURI());
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            String name = file.getName();
            if (!name.endsWith(".class")) {
                continue;
            }

            name = name.substring(0, name.length() - 6);
            String className = packageName + "." + name;
            Class<?> cl = classLoader.loadClass(className);

            //检查解析包中的类是否带有Controller注解
            Annotation controller = cl.getAnnotation(Controller.class);
            if (Objects.nonNull(controller)) {
                controllersMap.put(UUID.randomUUID().toString(), cl.newInstance());
            }
        }
        return controllersMap;
    }

    /**
     * 解析控制器上和方法上的RequestMapping注解，并存储对应的方法到容器
     *
     * @param controllersMap 控制器映射
     * @return RequestMapping的映射集合
     */
    public static Map<String, URIInfo> analysisURIByController(Map<String, Object> controllersMap) {
        Map<String, URIInfo> uriInfosMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : controllersMap.entrySet()) {
            String controllerId = entry.getKey();
            Object controller = entry.getValue();
            Class<?> controllerClass = controller.getClass();
            String uriPrefix = "";
            // 解析Controller类上的RequestMapping注解
            RequestMapping cRequestMappingAnnotation = controllerClass.getAnnotation(RequestMapping.class);
            if (Objects.nonNull(cRequestMappingAnnotation)) {
                uriPrefix = cRequestMappingAnnotation.value();
            }

            Method[] methods = controllerClass.getMethods();
            // 解析Controller类的方法上的RequestMapping注解
            for (Method controllerMethod : methods) {
                RequestMapping mRequestMappingAnnotation = controllerMethod.getAnnotation(RequestMapping.class);
                if (Objects.nonNull(mRequestMappingAnnotation)) {
                    String uri = uriPrefix + mRequestMappingAnnotation.value();
                    URIInfo uriInfo = new URIInfo(controllerId, controllerMethod);
                    uriInfosMap.put(uri, uriInfo);
                }
            }
        }
        return uriInfosMap;
    }

}
