package com.bosssoft.hr.train.jsp.example.controller.handler;

import com.alibaba.fastjson.JSON;
import com.bosssoft.hr.train.jsp.example.controller.entity.URIInfo;
import com.bosssoft.hr.train.jsp.example.controller.util.ReflectUtil;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.CastUtil;
import com.bosssoft.hr.train.jsp.example.util.Constraint;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Xx
 * @description 请求分发处理器
 * @create 2020-05-29 14:00
 * @since 1.0
 */
@Slf4j
public class HttpDispatchHandler extends HttpServlet {

    /**
     * 控制器容器
     */
    private static Map<String, Object> controllersMap;

    /**
     * URI请求容器
     */
    private static Map<String, URIInfo> uriInfosMap;

    @Override
    public void init() {
        initMapping(getInitParameter("controllerPackage"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String uri = req.getRequestURI().replace(req.getServletContext().getContextPath(), "");
        try {
            if (uri.equals("/")) {
                req.getRequestDispatcher("/index.jsp").forward(req, resp);
            } else {
                URIInfo uriInfo = uriInfosMap.get(uri);
                Object model = uriInfo.getMethod().invoke(controllersMap.get(uriInfo.getControllerId()), req, resp);
                if (Objects.nonNull(model) && model instanceof String) {
                    if (Objects.isNull(req.getSession().getAttribute(Constraint.USER_NAME))) {
                        req.getSession().setAttribute(Constraint.USER_NAME, req.getParameter("account"));
                    }
                    PrintWriter writer = resp.getWriter();
                    Map<String, String> map = new HashMap<>();
                    map.put("status", model.toString());
                    writer.write(JSON.toJSONString(map));
                    writer.flush();
                    writer.close();
                } else if (Objects.nonNull(model) && model instanceof List) {
                    List<User> list = CastUtil.cast(model);
                    req.getSession().setAttribute(Constraint.RESULT_SET, list);
                }
            }
        } catch (BusinessException e) {
            //处理最外层捕捉错误，并抛到前台
            log.error(e.toString());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            doGet(req, resp);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public static void initMapping(String packagePath) {
        try {
            controllersMap = ReflectUtil.analysisController(packagePath);
            uriInfosMap = ReflectUtil.analysisURIByController(controllersMap);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
