package com.bosssoft.hr.train.jsp.example.service;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;

import java.util.List;

/**
 *
 * @description 用户服务的接口
 * @author Xx
 * @create 2020-05-30 10:17
 * @since 1.0
 **/
public interface UserService {
    boolean save(User user);

    boolean remove(User user);

    boolean update(User user);

    List<User> queryByCondition(Query queryCondition);

    /**
     * 登录验证
     *
     * @param user 用户
     * @return  验证是否成功
     */
    boolean authentication(User user);
}
