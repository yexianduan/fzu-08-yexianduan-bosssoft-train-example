package com.bosssoft.hr.train.jsp.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *
 * @description
 * @author Administrator
 * @create 2020-05-22 14:52
 * @since 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private Integer id;
    private String name;
    private String code;
    private String password;

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public User(String code, String password) {
        this.code = code;
        this.password = password;
    }
}
