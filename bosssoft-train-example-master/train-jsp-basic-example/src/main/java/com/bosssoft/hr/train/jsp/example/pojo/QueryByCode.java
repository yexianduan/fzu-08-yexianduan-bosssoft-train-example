package com.bosssoft.hr.train.jsp.example.pojo;

/**
 *
 * @description 通过code查询
 * @author Xx
 * @create 2020/6/2 12:00
 * @since 1.0
 */
public class QueryByCode extends Query{

    public QueryByCode(String code) {
        super();
        this.sql += " where code = '" + code + "'";
    }
}
