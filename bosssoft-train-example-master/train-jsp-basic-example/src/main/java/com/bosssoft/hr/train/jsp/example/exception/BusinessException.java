package com.bosssoft.hr.train.jsp.example.exception;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @description 业务异常
 * @author Xx
 * @create 2020-05-30 10:32
 * @since 1.0
 */
@Getter
@Setter
public class BusinessException extends RuntimeException {
    private final String code;

    public BusinessException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    @Override
    public String toString() {
        return code + getMessage();
    }
}
