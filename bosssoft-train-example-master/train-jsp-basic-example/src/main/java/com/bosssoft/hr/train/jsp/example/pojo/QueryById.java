package com.bosssoft.hr.train.jsp.example.pojo;

/**
 *
 * @description  通过id查询
 * @author Xx
 * @create 2020/6/2 11:51
 * @since 1.0
 */
public class QueryById extends Query{

    public QueryById(Integer id) {
        super();
        this.sql += " where id = " + id;
    }
}
