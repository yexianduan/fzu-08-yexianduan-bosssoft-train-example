package com.bosssoft.hr.train.jsp.example.tag;

import com.bosssoft.hr.train.jsp.example.util.Constraint;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @description 定义<boss:userTag /> 标签
 * @author Xx
 * @create 2020-05-29 13:50
 * @since 1.0
 */
public class UserTag extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        PageContext context = (PageContext)this.getJspContext();
        String name = (String)context.getSession().getAttribute(Constraint.USER_NAME);
        JspWriter out = context.getOut();
        out.print("登录用户: " + name);
    }
}
