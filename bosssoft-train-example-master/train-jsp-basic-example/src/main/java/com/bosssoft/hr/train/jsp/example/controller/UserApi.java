package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;

import java.util.List;

/**
 *
 * @description
 * @author Xx
 * @create 2020-05-29 14:06
 * @since 1.0
 */
public interface UserApi {
    /**
     * 添加用户信息
     *
     * @param user 数据
     * @return  结果
     */
    String save(User user);

    /**
     * 删除用户信息
     *
     * @param user 数据
     * @return 结果
     */
    String remove(User user);

    /**
     * 更新用户信息
     *
     * @param user 数据
     * @return 结果
     */
    String update(User user);

    /**
     * 查询用户信息
     *
     * @param condition 条件
     * @return 结果
     */
    List<User> query(Query condition);
}
