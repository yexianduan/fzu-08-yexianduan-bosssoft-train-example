package com.bosssoft.hr.train.jsp.example.listener;

import com.bosssoft.hr.train.jsp.example.util.Constraint;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Objects;

/**
 *
 * @description 在线人数监听器
 * @author Xx
 * @create 2020/6/1 8:59
 * @since 1.0
 */
public class OnlineUserCountListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        ServletContext context = se.getSession().getServletContext();
        Integer count = (Integer)context.getAttribute(Constraint.ONLINE_COUNT);
        if (Objects.isNull(count)) {
            count = 1;
        } else {
            count += 1;
        }
        context.setAttribute(Constraint.ONLINE_COUNT, count);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ServletContext context = se.getSession().getServletContext();
        Integer count = (Integer)context.getAttribute(Constraint.ONLINE_COUNT);
        if (Objects.isNull(count)) {
            count = 0;
        } else {
            count -= 1;
        }
        context.setAttribute(Constraint.ONLINE_COUNT, count);
    }
}
