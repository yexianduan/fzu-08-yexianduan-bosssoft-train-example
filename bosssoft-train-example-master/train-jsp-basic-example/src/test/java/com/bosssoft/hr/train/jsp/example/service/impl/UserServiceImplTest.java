package com.bosssoft.hr.train.jsp.example.service.impl;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @description Service的测试类
 * @author Xx
 * @create 2020/6/2 16:00
 * @since 1.0
 */
@Slf4j
public class UserServiceImplTest {
    private final UserService userService = new UserServiceImpl();

    @Test
    public void save() {
        User user = new User(1 , "xx", "xx123", "xx123");
        assertTrue(userService.save(user));
    }

    @Test
    public void remove() {
        User user = new User(3 , "xx", "xx456", "xx456");
        userService.save(user);
        assertTrue(userService.remove(new User(3, null, null, null)));
    }

    @Test
    public void update() {
        User user = new User(4 , "zz", "yy123456", "yy123456");
        userService.save(user);
        assertTrue(userService.remove(new User(4, "xx", "yy123456", "yy123456")));
    }

    @Test
    public void queryByCondition() {
        List<User> list = userService.queryByCondition(new Query());
        assertNotNull(list);
        for (User user : list) {
            log.info(user.toString());
        }
    }

    @Test
    public void authentication() {
        User user = new User("xx123", "xx123");
        assertTrue(userService.authentication(user));
    }
}